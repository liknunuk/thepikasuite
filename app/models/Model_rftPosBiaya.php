<?php
class Model_rftPosBiaya
{
    private $table = "raftingCostPosting";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    // columns : rcpId() , pos , uraian
    // Basic CRUD OPERATION //

    // INSERTION
    public function tambah($data){
        $sql = "INSERT INTO $this->table SET pos=:pos , uraian=:uraian";
        $this->db->query($sql);
        $this->db->bind('pos',$data['pos']);
        $this->db->bind('uraian',$data['uraian']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data){
        $sql = "UPDATE $this->table SET pos=:pos , uraian=:uraian WHERE rcpId=rcpId";
        $this->db->query($sql);
        $this->db->bind('pos',$data['pos']);
        $this->db->bind('uraian',$data['uraian']);
        $this->db->bind('rpcId',$data['rpcId']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data){
        $sql = "DELETE FROM $this->table WHERE rcpId=:rcpId ";
        $this->db->query($sql);
        $this->db->bind('rcpId',$data['rcpId']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn=1){
        $row = ($pn -1 ) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY pos , uraian LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key){
        $sql = "SELECT * FROM $this->table WHERE rcpId=:key ";
        $this->db->query($sql);
        $this->db->bind('key',$key);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    // public function something($data){
    //     $sql = "";
    //     $this->db->query($sql);
    //     $this->db->bind('xxx',$data['xxx']);
    //     $this->db->bind('xxx',$xxx);
    //     return $this->db->resultSet();
    // }

}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
$this->db->execute();
return $this->db->resultSet();
return $this->db->resultOne();
*/