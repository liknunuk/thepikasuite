<?php
class Model_coa
{
    private $table = "chartOfAccount";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }

    
    // Basic CRUD OPERATION //

    // INSERTION
    // kode,arti
    public function tambah($data){
        $sql = "INSERT INTO $this->table SET kode=:kode , arti=:arti";
        $this->db->query($sql);
        $this->db->bind('kode',$data['kode']);
        $this->db->bind('arti',$data['arti']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // UPDATE
    public function ngubah($data){
        $sql = "UPDATE $this->table SET arti=:arti WHERE kode=:kode ";
        $this->db->query($sql);
        $this->db->bind('arti',$data['arti']);
        $this->db->bind('kode',$data['kode']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DELETE
    public function sampah($data){
        $sql = "DELETE FROM $this->table WHERE kode=:kode";
        $this->db->query($sql);
        $this->db->bind('kode',$data['kode']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // DISPLAY MULTIPLE
    public function tampil($pn=1){
        $row = ($pn -1 ) * rows;
        $sql = "SELECT * FROM $this->table ORDER BY kode LIMIT $row ," . rows;
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    // DISPLAY SINGULAR
    public function detail($key){
        $sql = "SELECT * FROM $this->table WHERE xxx=:key ";
        $this->db->query($sql);
        return $this->db->resultOne();
    }

    // CUSTOMIZED QUERY //
    // public function something($data){
    //     $sql = "";
    //     $this->db->query();
    //     $this->db->bind('xxx',$data['xxx']);
    //     $this->db->bind('xxx',$xxx);
    //     return $this->db->resultSet();
    // }

}

