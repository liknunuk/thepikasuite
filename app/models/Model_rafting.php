<?php
class Model_rafting
{
    // private $table = "namaTabel";
    private $db;

    public function __construct()
    {
        $this->db = new Database();
    }
    // manajemen pembayaran rafting
    // table: raftingCostIn
    // Kolom: bookingId , paketRaftingId , billing ,bdiscount

    public function payment($data)
    {
        // Data pembayaran / pemasukan rafting

        $billing = $data['price'] * $data['pax'];
        $sql = "INSERT INTO raftingCostIn SET paketRaftingId=:paketRaftingId , billing=:billing , bookingId=:bookingId";
        $this->db->query($sql);

        $this->db->bind('paketRaftingId', $data['trip']);
        $this->db->bind('billing', $billing);
        $this->db->bind('bookingId', $data['bookingId']);

        $this->db->execute();
        if ($this->setPaid($data) > 0) {
            return $this->db->rowCount();
        }
    }

    private function setPaid($data)
    {
        $sql = "UPDATE booking SET bookingStatus = 'paid' WHERE bookingId=:bookingId";
        $this->db->query($sql);
        $this->db->bind('bookingId', $data['bookingId']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    private function setClear($data)
    {
        $sql = "UPDATE booking SET bookingStatus = 'clear' WHERE bookingId=:bookingId";
        $this->db->query($sql);
        $this->db->bind('bookingId', $data['bookingId']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function activeBilling()
    {
        $sql = "SELECT raftingCostIn.bookingId , DATE_FORMAT(booking.tanggalMulai,'%d/%m/%Y') tanggalMulai, booking.namaPIC , booking.jumlahPerson , paketRafting.namaPaket , paketRafting.price , raftingCostIn.billing , raftingCostIn.discount FROM raftingCostIn , booking , paketRafting WHERE booking.bookingId = raftingCostIn.bookingId && paketRafting.paketRaftingId = raftingCostIn.paketRaftingId && bookingStatus='paid'";
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function paidBook($bookingId)
    {
        $sql = "SELECT raftingCostIn.bookingId , DATE_FORMAT(booking.tanggalMulai,'%d/%m/%Y') tanggalMulai, booking.namaPIC , booking.alamat , booking.jumlahPerson , paketRafting.namaPaket , paketRafting.price , raftingCostIn.billing , raftingCostIn.discount FROM raftingCostIn , booking , paketRafting WHERE raftingCostIn.bookingId=:bookingId && booking.bookingId = raftingCostIn.bookingId && paketRafting.paketRaftingId = raftingCostIn.paketRaftingId && bookingStatus='paid'";
        $this->db->query($sql);
        $this->db->bind('bookingId', $bookingId);
        return $this->db->resultOne();
    }

    public function setDiscount($data)
    {
        $sql = "UPDATE raftingCostIn SET discount=:discount WHERE bookingId=:bookingId";
        $this->db->query($sql);

        $this->db->bind('discount', $data['discount']);
        $this->db->bind('bookingId', $data['bookingId']);

        $this->db->execute();
        return $this->db->rowCount();
    }

    public function recalculate($data)
    {
        $sql = "DELETE FROM raftingCostIn WHERE bookingId=:bookingId";
        $this->db->query($sql);

        $this->db->bind('bookingId', $data['bookingId']);

        $this->db->execute();
        $rci = $this->db->rowCount();
        $clr = $this->setClear($data);
        return ($rci + $clr);
    }

    public function adtlCharge($bookingId)
    {
        $sql = "SELECT * FROM raftingAdtlCharge WHERE bookingId=:bookingId ORDER BY chargeIdx";
        $this->db->query($sql);
        $this->db->bind('bookingId', $bookingId);
        return $this->db->resultSet();
    }

    public function setAdtlCharge($data)
    {
        $sql = "INSERT INTO raftingAdtlCharge SET bookingId=:bookingId , uraian=:uraian , quantity=:quantity , indexHarga=:indexHarga";
        $this->db->query($sql);
        $this->db->bind('bookingId', $data['bookingId']);
        $this->db->bind('uraian', $data['uraian']);
        $this->db->bind('quantity', $data['quantity']);
        $this->db->bind('indexHarga', $data['indexHarga']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function reduceCrgIn($data)
    {
        $sql = "DELETE FROM raftingAdtlCharge WHERE chargeIdx=:chargeIdx";
        $this->db->query($sql);
        $this->db->bind('chargeIdx', $data['chargeIdx']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function nomorCashOut($bookingId)
    {
        $sql = "SELECT MAX( notesId ) nomor FROM raftingCostOut WHERE bookingId = :bookingId";
        $this->db->query($sql);
        $this->db->bind('bookingId', $bookingId);
        $result = $this->db->resultOne();

        return $result['nomor'] == NULL ? 1 : $result['nomor'] + 1;
    }

    public function setCashOut($data)
    {
        $sql = "INSERT INTO raftingCostOut SET bookingId=:bookingId , notesId=:notesId , posbiaya=:posbiaya , uraian=:uraian , quantity=:quantity , indexHarga=:indexHarga";
        $this->db->query($sql);
        $this->db->bind('bookingId', $data['bookingId']);
        $this->db->bind('notesId', $data['notesId']);
        $this->db->bind('posbiaya', $data['posbiaya']);
        $this->db->bind('uraian', $data['uraian']);
        $this->db->bind('quantity', $data['quantity']);
        $this->db->bind('indexHarga', $data['indexHarga']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function cashOut($bookingId)
    {
        $sql = "SELECT * FROM raftingCostOut WHERE bookingId=:bookingId ORDER BY posbiaya , notesId";
        $this->db->query($sql);
        $this->db->bind('bookingId', $bookingId);
        return $this->db->resultSet();
    }

    public function cashOutResume($bookingId)
    {
        $sql = "SELECT posbiaya,SUM(quantity*indexHarga)totalHarga FROM raftingCostOut WHERE bookingId=:bookingId GROUP BY posbiaya ORDER BY posbiaya";
        $this->db->query($sql);
        $this->db->bind('bookingId', $bookingId);
        return $this->db->resultSet();
    }

    public function costCancelation($data)
    {
        $sql = "DELETE FROM raftingCostOut WHERE bookingId =:bookingId && notesId=:notesId";
        $this->db->query($sql);
        $this->db->bind('bookingId', $data['bookingId']);
        $this->db->bind('notesId', $data['notesId']);
        $this->db->execute();
        return $this->db->rowCount();
    }

    // management paket rafting
    // tabel: paketRafting
    // kolom: paketRaftingId , namaPaket , deskripsi , price , miniper

    public function tampilPaket()
    {
        // Daftar Paket Rafting
        $sql = "SELECT * FROM paketRafting ORDER BY paketRaftingId";
        $this->db->query($sql);
        return $this->db->resultSet();
    }

    public function detailPaket($paketId)
    {
        $sql = "SELECT * FROM paketRafting WHERE paketRaftingId=:paketId";
        $this->db->query($sql);
        $this->db->bind('paketId', $paketId);
        return $this->db->resultOne();
    }

    public function tambahPaket($data)
    {
        $sql = "INSERT INTO paketRafting SET namaPaket=:namaPaket , deskripsi=:deskripsi , price=:price , miniper=:miniper , paketRaftingId=:paketRaftingId";
        $this->db->query($sql);
        $this->db->bind('namaPaket', $data['namaPaket']);
        $this->db->bind('deskripsi', $data['deskripsi']);
        $this->db->bind('price', $data['price']);
        $this->db->bind('miniper', $data['miniper']);
        $this->db->bind('paketRaftingId', $data['paketRaftingId']);

        $this->db->execute();
        return $this->db->rowCount();
    }

    public function ngubahPaket($data)
    {
        $sql = "UPDATE paketRafting SET namaPaket=:namaPaket , deskripsi=:deskripsi , price=:price , miniper=:miniper WHERE paketRaftingId=:paketRaftingId";

        $this->db->query($sql);
        $this->db->bind('namaPaket', $data['namaPaket']);
        $this->db->bind('deskripsi', $data['deskripsi']);
        $this->db->bind('price', $data['price']);
        $this->db->bind('miniper', $data['miniper']);
        $this->db->bind('paketRaftingId', $data['paketRaftingId']);

        $this->db->execute();
        return $this->db->rowCount();
    }

    public function mbuangPaket($data)
    {
        $sql = "DELETE FROM paketRafting WHERE paketRaftingId=:paketRaftingId";
        $this->db->query($sql);
        $this->db->bind('paketRaftingId', $data['paketRaftingId']);
        $this->db->execute();
        return $this->db->rowCount();
    }
}

// QUERY TEMPLATE
/*
$sql = "";
$this->db->query($sql);
$this->db->bind();
return $this->db->resultSet();
return $this->db->resultOne();

$this->db->execute();
return $this->db->rowCount();
*/