<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Booking extends Controller
{
  protected $activeMenu = 'index';
  // method default
  public function __construct()
  {
    if (!isset($_SESSION) || $_SESSION['loggedIn'] == false) {
      header("Location:" . BASEURL);
    }
  }

  public function index($pn = 1)
  {
    $data['booking'] = $this->model('Model_book')->tampil($pn);
    $this->activeMenu = 'index';
    $this->view('template/header');
    $this->view('template/pageHeader');
    $this->view('booking/book-navbar');
    $this->view('booking/index', $data);
    $this->view('template/footer');
  }

  public function statChange()
  {
    if ($this->model('Model_book')->bookingStatusUpdate($_POST) > 0) {
      echo "1";
    } else {
      echo "0";
    }
  }

  public function isActive($string)
  {
    if ($string == $this->activeMenu) {
      return "active";
    }
  }

  public function dmy($tanggal)
  {
    list($t, $b, $h) = explode("-", $tanggal);
    return "$h/$b/$t";
  }

  public function bookingStatus($data)
  {
    $bs = [
      'temp' => 'Belum Terkonfirmasi',
      'clear' => 'Terkonfirmasi',
      'paid' => 'Lunas',
      'cancel' => 'Batal',
      'done' => 'Selesai'
    ];

    return $bs[$data];
  }

  public function recap(int $tahun)
  {
    $data = [
      'booking' => $this->model('Model_book')->recap($tahun),
      'tahun' => $tahun
    ];
    $this->activeMenu = 'recap';
    $this->view('template/header');
    $this->view('template/pageHeader');
    $this->view('booking/book-navbar');
    $this->view('booking/recap', $data);
    $this->view('template/footer');
  }
}
