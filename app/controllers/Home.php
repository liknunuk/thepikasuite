<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Home extends Controller
{
  // method default
  public function index()
  {
    $this->view('template/header');
    $this->view('template/pageHeader');
    $this->view('forms/login');
    $this->view('template/footer');
  }
  public function auth()
  {
    $homepage = [
      'bkWoong' => 'kasWoong',
      'bkPikas' => 'KasPikas',
      'bkResto' => 'KasResto',
      'rafting' => 'Rafting',
      'djuragan' => 'User',
      'owner' => 'Booking'
    ];
    $data = $this->model('Model_persons')->authenticate($_POST);
    if (count($data) <= 1) {
      Alert::setAlert("Tidak Ditemukan", "Data User", "danger");
      header("Location:" . BASEURL);
    } else {
      if ($data['isadmin'] == "1") {
        $_SESSION['admin'] = true;
      } else {
        $_SESSION['admin'] = false;
      }
      $posjaga = $data['posjaga'];
      $_SESSION['loggedIn'] = true;
      $_SESSION['user'] = $data['fullName'];
      $_SESSION['posjaga'] = $posjaga;
      $_SESSION['homepage'] = $homepage[$posjaga];
    }
    header("Location: " . BASEURL . $homepage[$posjaga]);
  }

  public function metusikah()
  {
    session_unset();
    session_destroy();
    header("Location:" . BASEURL);
  }
}
