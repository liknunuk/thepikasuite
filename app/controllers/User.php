<?php
// sesuaikan nama kelas, tetap extends ke Controller
class User extends Controller
{
  public $activeMenu;

  public function __construct()
  {
    if (!isset($_SESSION) || $_SESSION['loggedIn'] == false) {
      header("Location:" . BASEURL);
    } else {
      if ($_SESSION['admin'] == false) {
        header("Location:" . BASEURL . "Booking");
      }
    }
  }
  // method default
  public function index($nh = 1)
  {
    $this->activeMenu = 'index';
    $data['user'] = $this->model('Model_persons')->tampil($nh);
    $this->view('template/header');
    $this->view('template/pageHeader');
    $this->view('user/user-navbar');
    $this->view('user/index', $data);
    $this->view('template/footer');
    // echo $_SESSION['posjaga'];
  }

  public function baru()
  {
    $this->activeMenu = 'tambah';
    $this->view('template/header');
    $this->view('template/pageHeader');
    $this->view('user/user-navbar');
    $this->view('forms/persons');
    $this->view('template/footer');
  }

  public function info($uid)
  {
    $data['user'] = $this->model('Model_persons')->detail($uid);
    $this->view('template/header');
    $this->view('template/pageHeader');
    $this->view('user/user-navbar');
    $this->view('user/user-info', $data);
    $this->view('template/footer');
  }

  public function setUser()
  {

    if ($this->model('Model_persons')->tambah($_POST) > 0) {
      Alert::setAlert('berhasil ditambahkan', 'user', 'success');
    } else {
      Alert::setAlert('gagal ditambahkan', 'user', 'danger');
    }
    header("Location:" . BASEURL . "User");
  }

  public function chgName()
  {
    if ($this->model('Model_persons')->ngubah($_POST) > 0) {
      Alert::setAlert('berhasil diubah', 'Nama pengguna', 'success');
    } else {
      Alert::setAlert('berhasil diubah', 'Nama pengguna', 'success');
    }
    header("Location:" . BASEURL . "User");
  }

  public function userPass($userId)
  {
    $data['userId'] = $userId;
    $this->view('template/header');
    $this->view('template/pageHeader');
    $this->view('user/user-navbar');
    $this->view('forms/adm-password', $data);
    $this->view('template/footer');
  }

  public function chPass()
  {
    if ($this->model('Model_persons')->chPasswd($_POST) > 0) {
      Alert::setAlert('gagal diubah', 'Password', 'danger');
    } else {
      Alert::setAlert('berhasil diubah', 'Password', 'success');
    }
    header("Location:" . BASEURL . "User/info/{$_POST['userId']}");
  }

  public function pecat()
  {
    if ($this->model('Model_persons')->sampah($_POST) > 0) {
      Alert::setAlert('gagal dihapus', 'User', 'danger');
      echo "1";
    } else {
      Alert::setAlert('berhasil dihapus', 'User', 'success');
      echo "0";
    }
  }

  public function swadmin()
  {
    if ($this->model('Model_persons')->switchAdmin($_POST) > 0) {
      echo "1";
    } else {
      echo "0";
    }
  }

  public function cari($fullName)
  {
    $users = $this->model('Model_persons')->cariUser($fullName);
    echo json_encode($users, JSON_PRETTY_PRINT);
  }


  public function posisiUser($kode)
  {
    switch ($kode) {
      case '1':
        return 'Administratur';
        break;
      case '0':
        return 'User Reguler';
        break;
      default:
        return 'User Reguler';
        break;
    }
  }

  public function isActive($string)
  {
    if ($string == $this->activeMenu) {
      return "active";
    }
  }
  //   private functions
}
