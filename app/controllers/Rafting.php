<?php
// sesuaikan nama kelas, tetap extends ke Controller
class Rafting extends Controller
{
  public function __construct()
  {
    if (!isset($_SESSION) || $_SESSION['loggedIn'] == false) {
      header("Location:" . BASEURL);
    }
  }

  // method default
  public function index()
  {
    $data['booking'] = $this->model('Model_book')->raftingBook();
    $data['paket'] = $this->model('Model_rafting')->tampilPaket();
    $this->view('template/header');
    $this->view('template/pageHeader');
    $this->view('raft/raft-navbar', $data);
    $this->view('raft/index', $data);
    $this->view('template/footer');
  }

  public function payment()
  {
    // print_r($_POST);
    if ($this->model("Model_rafting")->payment($_POST) > 0) {
      Alert::setAlert('berhasil dicatat', 'Pembayaran', 'success');
    } else {
      Alert::setAlert('gagal dicatat', 'Pembayaran', 'danger');
    }
    header("Location:" . BASEURL . "Rafting/billinglist");
  }

  public function billinglist()
  {
    $data['billing'] = $this->model('Model_rafting')->activeBilling();
    $this->view('template/header');
    $this->view('template/pageHeader');
    $this->view('raft/raft-navbar', $data);
    $this->view('raft/billing', $data);
    $this->view('template/footer');
  }

  public function setDiskon()
  {
    // print_r($_POST);
    // Array ( [bookingId] => 1370CAB.50 [discount] => 10000 [jmPersonel] => 24 )
    $discount = $_POST['discount'] * $_POST['jmPersonel'];
    $data = ['bookingId' => $_POST['bookingId'], 'discount' => $discount];
    if ($this->model('Model_rafting')->setDiscount($data) > 0) {
      Alert::setAlert('berhasil ditambahkan', 'Diskon', 'success');
    } else {
      Alert::setAlert('gagal ditambahkan', 'Diskon', 'danger');
    }
    header("Location: " . BASEURL . "Rafting/billingList");
  }

  public function recalculate()
  {
    if ($this->model('Model_rafting')->recalculate($_POST) > 1) {
      echo "1";
    } else {
      echo "0";
    }
  }

  public function woeroeng()
  {
    $costIn = $this->model('Model_rafting')->recalculate($_POST);
    $bookin = $this->model('Model_book')->sampah($_POST);
    if (($costIn + $bookin) > 2) {
      echo "1";
    } else {
      echo "0";
    }
  }

  public function expenditure($bookingId)
  {
    $data['booking'] = $this->model('Model_rafting')->paidBook($bookingId);
    $data['posbiaya'] = $this->model('Model_rftPosBiaya')->tampil(1);
    $data['danamara'] = $this->model('Model_rafting')->adtlCharge($bookingId);
    $data['danametu'] = $this->model('Model_rafting')->cashOut($bookingId);
    $data['ikhtisar'] = $this->model('Model_rafting')->cashOutResume($bookingId);

    $this->view('template/header');
    $this->view('template/pageHeader');
    $this->view('raft/raft-navbar', $data);
    $this->view('raft/expenditure', $data);
    $this->view('template/footer');
  }

  public function invoice($bookingId)
  {
    $data['booking'] = $this->model('Model_rafting')->paidBook($bookingId);
    // $data['posbiaya']= $this->model('Model_rftPosBiaya')->tampil(1);
    // $data['danamara']= $this->model('Model_rafting')->adtlCharge($bookingId);
    // $data['danametu']= $this->model('Model_rafting')->cashOut($bookingId);
    // $data['ikhtisar']= $this->model('Model_rafting')->cashOutResume($bookingId);

    // $this->view('template/header');
    // $this->view('template/pageHeader');
    // $this->view('raft/raft-navbar',$data);
    $this->view('raft/invoice', $data);
    // $this->view('template/footer');
  }

  public function repExpdt($bookingId)
  {
    $data['booking'] = $this->model('Model_rafting')->paidBook($bookingId);
    $data['posbiaya'] = $this->model('Model_rftPosBiaya')->tampil(1);
    $data['danamara'] = $this->model('Model_rafting')->adtlCharge($bookingId);
    $data['danametu'] = $this->model('Model_rafting')->cashOut($bookingId);
    $data['ikhtisar'] = $this->model('Model_rafting')->cashOutResume($bookingId);

    // script export
    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=Rafting_" . $data['booking']['bookingId'] . "-" . $data['booking']['namaPIC'] . ".xls");

    $this->view('template/header');
    // $this->view('template/pageHeader');
    // $this->view('raft/raft-navbar',$data);
    $this->view('raft/reportExpdt', $data);
    $this->view('template/footer');
  }

  public function setPosbiaya($bookId)
  {
    if ($this->model('Model_rftPosBiaya')->tambah($_POST) > 0) {
      Alert::setAlert('berhasil ditambahkan', 'pos biaya rafting ', 'success');
    } else {
      Alert::setAlert('gagal ditambahkan', 'pos biaya rafting ', 'danger');
    }
    header("Location: " . BASEURL . "Rafting/expenditure/" . $bookId);
  }

  public function setAdtlCharge()
  {
    if ($this->model('Model_rafting')->setAdtlCharge($_POST) > 0) {
      Alert::setAlert('berhasil ditambahkan', 'biaya tambahan rafting ', 'success');
    } else {
      Alert::setAlert('gagal ditambahkan', 'biaya tambahan rafting ', 'danger');
    }
    header("Location: " . BASEURL . "Rafting/expenditure/" . $_POST['bookingId']);
  }

  public function reduceCrgIn()
  {
    if ($this->model('Model_rafting')->reduceCrgIn($_POST) > 0) {
      echo "1";
    } else {
      echo "0";
    }
    // header("Location: ". BASEURL . "Rafting/expenditure/" . $_POST['bookingId']);
  }

  public function nomorCashOut($bookingId)
  {
    echo $this->model('Model_rafting')->nomorCashOut($bookingId);
  }

  public function costCancelation()
  {
    echo $this->model('Model_rafting')->costCancelation($_POST) > 0 ? "1" : "0";
  }

  public function setExpenditure()
  {
    if ($this->model('Model_rafting')->setCashOut($_POST) > 0) {
      Alert::setAlert('berhasil ditambahkan', 'biaya pengeluaran rafting ', 'success');
    } else {
      Alert::setAlert('gagal ditambahkan', 'biaya pengeluaran rafting ', 'danger');
    }
    header("Location: " . BASEURL . "Rafting/expenditure/" . $_POST['bookingId']);
  }

  public function raftDetail($bookid)
  {
    $data['booking'] = $this->model('Model_book')->detail($bookid);
    echo json_encode($data['booking'], JSON_PRETTY_PRINT);
  }

  public function write()
  {
    $data['calc'] = $_POST;
    // $data['item'] = $this->raftChargeItems();
    $this->view('raft/raftcalc', $data);
  }

  public function dmy($tanggal)
  {
    list($t, $b, $h) = explode("-", $tanggal);
    return "$h/$b/$t";
  }

  public function isActive($string)
  {
    if ($string == $this->activeMenu) {
      return "active";
    }
  }
}
