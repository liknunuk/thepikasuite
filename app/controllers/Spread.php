<?php
class Spread extends Controller
{
    public function index($pos, $tahun, $bulan)
    {
        $data = [
            'title' => 'Laporan Kas',
            'pos' => $pos,
            'tahun' => $tahun,
            'bulan' => $bulan
        ];
        switch ($pos) {
            case 'resto':
                $data['kas'] = $this->kasResto($tahun, $bulan);
                break;
            case 'pikas':
                $data['kas'] = $this->kasPikas($tahun, $bulan);
                break;
            case 'woong':
                $data['kas'] = $this->kasWoong($tahun, $bulan);
                break;
        }

        // script export
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=Kas_" . $pos . "_{$tahun}_{$bulan}" . ".xls");

        $this->view('template/header', $data);
        $this->view('spread/index', $data);
        $this->view('template/footer');
    }

    private function kasResto($tahun, $bulan)
    {
        $periode = "{$tahun}-{$bulan}";
        $data['bln'] = $this->model('Model_kbResto')->rekapBulanan($periode);
        $data['sld'] = $this->model('Model_kbResto')->rekapTahunan($tahun);
        return $data;
    }
    private function kasPikas($tahun, $bulan)
    {
        $periode = "{$tahun}-{$bulan}";
        $data['bln'] = $this->model('Model_kbPikas')->rekapBulanan($periode);
        $data['sld'] = $this->model('Model_kbPikas')->rekapTahunan($tahun);
        return $data;
    }
    private function kasWoong($tahun, $bulan)
    {
        $periode = "{$tahun}-{$bulan}";
        $data['bln'] = $this->model('Model_kbWoong')->rekapBulanan($periode);
        $data['sld'] = $this->model('Model_kbWoong')->rekapTahunan($tahun);
        return $data;
    }
}
