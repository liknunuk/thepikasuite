<div class="row">
    <div class="col-lg-12">
        <div class="page-title">
            <h3>DATA BOOKING TAHUN <?= $data['tahun']; ?></h3>
        </div>
        <div class="table-responsive">
            <table class="table table-sm table-bordered">
                <thead>
                    <tr>
                        <th>Booking ID</th>
                        <th>Paket</th>
                        <th>Tanggal</th>
                        <th>Customer</th>
                        <th>Objek Booking</th>
                        <th>Harga</th>
                        <th>Booking Status</th>
                    </tr>
                </thead>
                <!-- 
                [bookingId] => 135F73D.58
                [paketRaftingId] => BW-SR5
                [billing] => 6660000
                [discount] => 540000
                [tgBooking] => 2022-01-05 09:38:12
                [price] => 6120000
                [wahana] => Rafting
                [namaPIC] => SKB Banjarnegara
                [bookingStatus] => paid
                 -->
                <tbody>
                    <?php foreach ($data['booking'] as $booking) : ?>
                        <tr>
                            <td><?= $booking['bookingId']; ?></td>
                            <td><?= $booking['paketRaftingId']; ?></td>
                            <td><?= $booking['tgBooking']; ?></td>
                            <td><?= $booking['namaPIC']; ?></td>
                            <td><?= $booking['wahana']; ?></td>
                            <td class='text-right' style='font-family:monospace;'>
                                <?= number_format($booking['billing'], 0, ',', '.'); ?><br />
                                <?= number_format($booking['discount'], 0, ',', '.'); ?><br />
                                <?= number_format($booking['price'], 0, ',', '.'); ?>
                            </td>
                            <td><?= ucfirst($booking['bookingStatus']); ?></td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>

    </div>
</div>

<?php $this->view('template/bs4js'); ?>