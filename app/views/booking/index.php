<div class="row">
    <div class="col-lg-12">
        <div class="page-title">
            <h3>TAMU BOOKING</h3>
        </div>
        <div class="table-responsive">
            <table class="table table-striped sm">
                <thead>
                    <tr>
                        <th>Tanggal</th>
                        <th>Booking</th>
                        <th>Pemesan</th>
                        <th>Jumlah</th>
                        <th>Status</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($data['booking'] as $booking): ?>
                    <tr>
                        <td>
                            <?=$this->dmy($booking['tanggalMulai']);?><br/>
                            <?="jam " . $booking['jam'];?>
                        </td>
                        <td>
                        <div class="list-group">
                        <?php
                        $wahana = explode(",",$booking['wahana']);
                        foreach($wahana as $w): ?>
                            <li class="list-group-item py-0 my-0 border-0"><?=$w?></li>
                        <?php endforeach; ?>
                        </div>
                        </td>
                        <td><?=$booking['namaPIC'];?>&nbsp;<br/><?=$booking['alamat'];?>&nbsp;<br/><?=$booking['telepon'];?></td>
                        <td><?=$booking['jumlahPerson'];?></td>
                        <td>
                            <a href="javascript:void(0)" class="btn btn-info bookingstatus" id="<?=$booking['bookingId'];?>">
                                <?=$this->bookingStatus($booking['bookingStatus']);?>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
            
        </div>
    
    </div>
</div>
<!-- modals -->

<!-- Moodal Booking Status -->
<div class="modal" tabindex="-1" role="dialog" id="modalBookingStatus">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Booking Status</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <h6 id="mdbsBookingMessage" class="text-center"></h6>
        <span id="mdbsBookingId" style="display:none;"></span>
        <ul class="list-group text-center">
            <li class="list-group-item">
                <a href="#" class="btn btn-info">
                    <i class="bg-transparent bookstat fas fa-check" id="clear">&nbsp;Confirmed</i>
                </a>
            </li>
            <li class="list-group-item">
                <a href="#" class="btn btn-danger">
                    <i class="bg-transparent bookstat fas fa-times" id="cancel">&nbsp;Canceled</i>
                </a>
            </li>
            <li class="list-group-item">
                <a href="#" class="btn btn-success">
                    <i class="bg-transparent bookstat fas fa-flag-checkered" id="done">&nbsp;Finished</i>
                </a>
            </li>
        </ul>
      </div>
      <div class="modal-footer">
        <!-- <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
      </div>
    </div>
  </div>
</div>
<!-- Moodal Booking Status -->

<!-- modals -->

<?php $this->view('template/bs4js');?>
<script>
    $('.bookingstatus').on('click',function(){
        // alert( $(this).prop('id') );
        let bookingId=$(this).prop('id'),
            wahana  = $(this).parent().parent().children('td:nth-child(2)').text(),
            client  = $(this).parent().parent().children('td:nth-child(3)').text(),
            pesan   = "";

        pesan = "Booking nomor " + bookingId + " <br/><small>atas nama </small><br/>" + client ;
        $('#modalBookingStatus').modal('show');
        $('#mdbsBookingMessage').html( pesan );
        $('#mdbsBookingId').text( bookingId );
    })

    $('.bookstat').on('click', function(){
        // 'clear','cancel','done')
        let bookId , bookstatus;
        bookId = $("#mdbsBookingId").text();
        bookStatus = $(this).prop('id');
        // console.log(bookId, bookStatus);
        $.post('<?=BASEURL;?>Booking/statChange',{
            bookingId       : bookId,
            bookingStatus   : bookStatus
        },function(resp){
            if(resp == '1' ) location.reload();
        })
    })
</script>