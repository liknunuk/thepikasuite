<div class="row bg-dark">
  <div class="col-sm-12 py-0">
    <nav class="navbar navbar-expand-sm navbar navbar-dark bg-dark">
      <a class="navbar-brand" href="#">The Pikas</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
          <!-- <li class="nav-item <?= $this->isActive('index');?>">
            <a class="nav-link" href="<?=BASEURL;?>Bukukas">Buku Kas <span class="sr-only">(current)</span></a>
          </li> -->

          <?php $this->view('home/sysnav'); ?>
          
          <li class="nav-item dropdown <?=$this->isActive('index');?>">
            <a href="#" class="nav-link dropdown-toggle" id="bkDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expandes="false">Buku Kas Besar</a>
            <div class="dropdown-menu" aria-labelledby="bkDropdown">
              <a href="<?=BASEURL;?>KasPikas" class="dropdown-item">Kas Besar Pikas</a>
              <a href="<?=BASEURL;?>KasResto" class="dropdown-item">Kas Besar Resto</a>
              <a href="<?=BASEURL;?>KasWoong" class="dropdown-item">Kas Besar Banyuwoong</a>
            </div>
          </li>

          <!--           
          <li class="nav-item< ?= $this->isActive('tambah');?>"  id="nambahcoa">
            <a class="nav-link" href="javascript:void(0)">Tambah CoA</a>
          </li> 
          -->

          <li class="nav-item dropdown <?= $this->isActive('coa');?>">
            <a class="nav-link dropdown-toggle" href="#" id="coaDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              CoA
            </a>
            <div class="dropdown-menu" aria-labelledby="coaDropdown">
              <a class="dropdown-item" id="nambahcoa" href="javascript:void(0)">Tambah CoA</a>
              <a class="dropdown-item" href="<?=BASEURL;?>Bukukas/coa">Kelola CoA</a>
              <div class="dropdown-divider"></div>
              <!-- <a class="dropdown-item" href="#">Something else here</a> -->
            </div>
          </li>

          <li class="nav-item <?=$this->isActive('rekap');?>" id="rekap">
            <a href="<?=BASEURL.$data['controller'];?>/rekap" class="nav-link">Rekap</a>
          </li>

          <?php $this->view('home/adminmenu'); ?>
          <!-- <li class="nav-item">
            <a href="<?=BASEURL;?>User" class="nav-link">User</a>
          </li> -->

          <!--
          <li class="nav-item">
            <a class="nav-link disabled" href="#">Disabled</a>
          </li>
          -->
        </ul>
        <?php if($data['controller']!='coa'):?>
        <form class="form-inline my-2 my-lg-0" action="<?=BASEURL.$data['controller'];?>/caritrx" method="post">
          <input type="hidden" name="col" value="keterangan">
          <input name="key" class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Cari</button>
        </form>
        <?php endif; ?>
      </div>
        <ul class="navbar-nav">
          <li class="nav-item"><a href="<?=BASEURL;?>Home/metusikah" class="nav-link">
            <i class="fas fa-sign-out-alt bg-danger" style="color:white;"></i>
          </a></li>
        </ul>      
    </nav>
  </div>
</div>