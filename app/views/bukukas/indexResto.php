<div class="row">
    <div class="col-lg-12">
        <?php Alert::sankil(); ?>
    </div>
</div>
<div class="row">
    <div class="col-sm-4">
        <div class="page-title">
            <h3>Chart Of Account<br /><small>Kode Posting Pembukuan</small></h3>
        </div>
        <span class="info">Klik / tap kode untuk memasukkan kode ke blanko pencatatan</span>
        <table class="table table-sm">
            <thead>
                <tr>
                    <th>Kode</th>
                    <th>Arti</th>
                </tr>
            </thead>
            <tbody id="listOfCoa">
                <?php foreach ($data['coa'] as $coa) : ?>
                    <tr>
                        <td>
                            <b>
                                <span class="kodeCoa"><?= $coa['kode']; ?></span>
                            </b>
                        </td>
                        <td><?= $coa['arti']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="col-sm-8">
        <div class="page-title">
            <h3>Blanko Pencatatan Pembukuan</h3>
            <h4>Pikasto</h4>
        </div>
        <!-- form pembukuan -->

        <?php $this->view('forms/bukuKas', $data); ?>

        <!-- transaksi harian -->
        <div class="col-sm-12">
            <div class="page-title">
                <h3>Catatan Transaksi Keuangan Tanggal <?= date('d/m/Y'); ?></h3>
            </div>
            <div class="table-responsive">

                <table class="table table-sm table-striped">
                    <thead>
                        <th>Uraian</th>
                        <th>Masuk</th>
                        <th>Keluar</th>
                    </thead>
                    <tbody id="listOfTrx">
                        <?php
                        $totalDebet = 0;
                        $totalKredit = 0;
                        foreach ($data['trx'] as $trx) :
                        ?>
                            <tr>
                                <td>
                                    <div class="trxNote"><?= $trx['kasId']; ?>-<?= $trx['kodePos']; ?></div>
                                    <?= $trx['keterangan']; ?>
                                </td>
                                <td class='text-right px-2'><?= number_format($trx['debet'], 2, ',', '.'); ?></td>
                                <td class='text-right px-2'><?= number_format($trx['kredit'], 2, ',', '.'); ?></td>
                            </tr>
                        <?php
                            $totalDebet += $trx['debet'];
                            $totalKredit += $trx['kredit'];
                        endforeach;
                        ?>
                        <tr class='bg-summary'>
                            <td class='text-center'>Jumlah Keseluruhan</td>
                            <td class="text-right px-2"><?= number_format($totalDebet, 2, ',', '.'); ?></td>
                            <td class="text-right px-2"><?= number_format($totalKredit, 2, ',', '.'); ?></td>
                        </tr>
                        <?php $sisaldo = $totalDebet - $totalKredit; ?>
                        <tr class='bg-result'>
                            <td>Sisa / Saldo</td>
                            <td class="text-right" colspan="2"><?= number_format($sisaldo, 2, ',', '.'); ?></td>
                        </tr>
                    </tbody>
                </table>

            </div>
        </div>
    </div>
</div>
<!-- <div class="row">
    <div class="col-sm-12">
        <div class="page-title">
            <h3>Catatan Transaksi Keuangan Tanggal <?= date('d/m/Y'); ?></h3>
        </div>
    </div>
</div> -->
<!-- modals -->

<!-- Modal COA -->
<div class="modal" tabindex="-1" role="dialog" id="modalCoa">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Chart Of Account</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="<?= BASEURL; ?>Bukukas/setcoa" method="post" class="form-horizontal">
                    <div class="form-group row">
                        <label for="mdckode" class="col-sm-3">Kode Coa</label>
                        <div class="col-sm-9">
                            <input type="number" name="kode" id="mdckode" class="form-control" min='10000' value="10000">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="mdcarti" class="col-sm-3">Arti</label>
                        <div class="col-sm-9">
                            <input type="text" name="arti" id="mdcarti" class="form-control" maxlength="45" placeHolder="info singkat" required>
                        </div>
                    </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Simpan</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </form>
            </div>
        </div>
    </div>
</div>


<!-- modals -->
<?php $this->view('template/bs4js'); ?>
<script src="<?= BASEURL . "js/kontrol-kas.js"; ?>"></script>
<script>
    $('.kodeCoa').on('click', function() {
        let kode = $(this).text();
        $('#bkKodePos').val(kode);
    })
    $('#nambahcoa').on('click', function() {
        $('#modalCoa').modal('show');
    })
</script>