<?php
function tanggalan($data)
{
    list($t, $b, $h) = explode("-", $data);
    return "$h-$b-$t";
}
$totalDebet = 0;
$totalKredit = 0;
?>
<!-- 
    [tanggal] => 2022-01-05 
    [kodePos] => 11000 
    [arti] => Tunai Masuk 
    [keterangan] => Investasi PT. Dolan Dolan, Jayapura 
    [debet] => 100000000 
    [kredit] => 0
 -->
<table width="100%" border="1" cellspacing="0" cellpadding="0">
    <thead>
        <tr>
            <th colspan="5"><?php echo "Kas " . ucfirst($data['pos']) . " Bulan {$data['bulan']} Tahun {$data['tahun']}"; ?></th>
        </tr>
        <tr>
            <th>Tanggal</th>
            <th>Pos</th>
            <th>Keterangan</th>
            <th>Masuk</th>
            <th>Keluar</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($data['kas']['bln'] as $kas) : ?>
            <tr>
                <td><?= tanggalan($kas['tanggal']); ?></td>
                <td>[<?= $kas['kodePos']; ?>] <small><?= $kas['arti']; ?></small></td>
                <td><?= $kas['keterangan']; ?></td>
                <td align='right'><?= number_format($kas['debet'], 0, ',', '.'); ?></td>
                <td align='right'><?= number_format($kas['kredit'], 0, ',', '.'); ?></td>
            </tr>
        <?php
            $totalDebet += $kas['debet'];
            $totalKredit += $kas['kredit'];
        endforeach;
        ?>
        <tr>
            <td colspan="3" align="right">Jumlah Keseluruhan</td>
            <td align='right'><?= number_format($totalDebet, 0, ',', '.'); ?></td>
            <td align='right'><?= number_format($totalKredit, 0, ',', '.'); ?></td>
        </tr>
        <tr>
            <td colspan="3" align="right">Pendapatan Bulanan</td>
            <td colspan="2" align='right'><?= number_format(($totalDebet - $totalKredit), 0, ',', '.'); ?></td>

        </tr>
        <tr>
            <td colspan="3" align="right">Keuangan Tahun Berjalan</td>
            <td align='right'><?= number_format($data['kas']['sld']['debet'], 0, ',', '.'); ?></td>
            <td align='right'><?= number_format($data['kas']['sld']['kredit'], 0, ',', '.'); ?></td>

        </tr>
        <tr>
            <td colspan="3" align="right">Saldo Tahun Berjalan</td>
            <td colspan="2" align='right'><?= number_format(($data['kas']['sld']['debet'] - $data['kas']['sld']['kredit']), 0, ',', '.'); ?></td>

        </tr>
    </tbody>
    <!-- Array ( [debet] => 125000000 [kredit] => 10000000 [saldo] => 115000000 ) -->
</table>