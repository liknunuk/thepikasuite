<div class="row">
  <div class="col-lg-12">
    <div class="page-title">
      <h3>Pencatatan Biaya Rafting</h3>
      <?php Alert::sankil(); ?>
    </div>
  </div>
</div>
<div class="row">
  <div class="col-lg-8 mx-auto">
    <table class="table table-sm table-bordered">
      <tbody>
        <tr>
          <td>Nomor Booking</td>
          <td><?= $data['booking']['bookingId']; ?></td>
          <td>Tarif / Pax</td>
          <td class="text-right"><?= number_format($data['booking']['price'], 2, ',', '.'); ?></td>
        </tr>
        <tr>
          <td>Tanggal</td>
          <td><?= $data['booking']['tanggalMulai']; ?></td>
          <td>Tagihan</td>
          <td class="text-right"><?= number_format($data['booking']['billing'], 2, ',', '.'); ?></td>
        </tr>
        <tr>
          <td>Group/PIC</td>
          <td><?= $data['booking']['namaPIC']; ?></td>
          <td>Diskon</td>
          <td class="text-right"><?= number_format($data['booking']['discount'], 2, ',', '.'); ?></td>
        </tr>
        <tr>
          <td>Jumlah Perserta</td>
          <td><?= $data['booking']['jumlahPerson']; ?></td>
          <td>Total Anggaran</td>
          <td class="text-right">
            <?php
            $budget = $data['booking']['billing'] - $data['booking']['discount'];
            echo number_format($budget, 2, ',', '.');
            ?>
          </td>
        </tr>
        <tr>
          <td>Paket Trip</td>
          <td><?= $data['booking']['namaPaket']; ?></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
        <tr>
          <td colspan="4" class="text-center">
            <a target="_blank" href="<?= BASEURL; ?>Rafting/invoice/<?= $data['booking']['bookingId']; ?>">Cetak Invoice</a>
          </td>
        </tr>
      </tbody>
    </table>
  </div>
</div>
<div class="row">
  <div class="col-lg-3">
    <h4>Pos Pengeluaran</h4>
    <form action="<?= BASEURL . 'Rafting/setPosbiaya/' . $data['booking']['bookingId']; ?>" method="post">

      <div class="form-group">
        <label for="pbPos">Pos Biaya</label>
        <select name="pos" id="pbPos" class="form-control">
          <option value="Crew">Crew Rafting</option>
          <option value="Tamu">Tamu Rafting</option>
          <option value="Transport">Transportasi</option>
          <option value="Lainnya">Pos Lainnya</option>
        </select>
      </div>
      <div class="form-group">
        <label for="pbUraian">Uraian</label>
        <input type="text" name="uraian" id="pbUraian" class="form-control">
      </div>
      <div class="form-group text-center">
        <button type="submit" class="btn btn-success">Catat!</button>
      </div>
    </form>
    <table class="table table-sm table-bordered">
      <thead>
        <tr>
          <th>Pos</th>
          <th>Uraian</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($data['posbiaya'] as $pb) : ?>
          <tr>
            <td><?= $pb['pos']; ?></td>
            <td><a href="javascript:void(0)" class="posbiaya"><?= $pb['uraian']; ?></a></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
  <div class="col-lg-9">
    <h4>Penambahan Biaya / Additionan Charge</h4>
    <!-- bookingId , uraian , quantity , indexHarga -->
    <table class="table table-sm table-striped">
      <thead>
        <form action="<?= BASEURL . 'Rafting/setAdtlCharge/'; ?>" method="post">
          <input type="hidden" name="bookingId" value="<?= $data['booking']['bookingId']; ?>">
          <tr>
            <td><input type="text" name="uraian" id="adduraian" class="form-control" required></td>
            <td width="125"><input type="number" name="quantity" id="quantity" class="form-control" min=1 value=1></td>
            <td width="200"><input type="number" name="indexHarga" id="indexHarga" class="form-control" min=1 value=1></td>
            <td width="125"><button type="submit" class="btn btn-success form-control">Catat!</button></td>
          </tr>
        </form>
        <tr>
          <th>Uraian</th>
          <th>Banyaknya</th>
          <th>Index Harga</th>
          <th>Jumlah Harga</th>
        </tr>
      </thead>
      <tbody>
        <?php
        $totalBiayaTambahan = 0;
        foreach ($data['danamara'] as $masuk) :
        ?>
          <tr>
            <td><span class="mr-3"><i class="fas fa-eraser crgIn" style="font-size: 24px; color:red; cursor:pointer" id="<?= $masuk['chargeIdx']; ?>"></i></span><?= $masuk['uraian']; ?></td>
            <td class="text-right"><?= $masuk['quantity']; ?></td>
            <td class="text-right"><?= number_format($masuk['indexHarga'], 2, ',', '.'); ?></td>
            <?php $jumlah = $masuk['quantity'] * $masuk['indexHarga']; ?>
            <td class="text-right"><?= number_format($jumlah, 2, ',', '.'); ?></td>
          </tr>
        <?php
          $totalBiayaTambahan += $jumlah;
        endforeach;
        ?>
        <tr>
          <th colspan="3">Sub Total Additional Charges</th>
          <th class="text-right"><?= number_format($totalBiayaTambahan, 2, ',', '.'); ?></th>
        </tr>
      </tbody>
    </table>

    <h4 id="formCashOut">Uraian Pengeluaran</h4>
    <!-- bookingId , notesId , posbiaya , uraian , quantity , indexHarga -->
    <table class="table table-sm table-striped">
      <thead>
        <tr>
          <form action="<?= BASEURL; ?>Rafting/setExpenditure" method="post">
            <td width="75">
              <input type="text" class="form-control" id="notesId" name="notesId">
            </td>
            <td>
              <select name="posbiaya" id="posbiaya" class="form-control">
                <option value="Crew">Crew Rafting</option>
                <option value="Tamu">Tamu Rafting</option>
                <option value="Transport">Transportasi</option>
                <option value="Lainnya">Pos Lainnya</option>
              </select>
            </td>
            <td>
              <input type="text" name="uraian" id="uraian" class="form-control" required>
            </td>
            <td width="125">
              <input type="number" name="quantity" id="quantity" class="form-control" required min=1 value=1>
            </td>

            <td width="200">
              <input type="number" name="indexHarga" id="indexHarga" class="form-control" required min=0 value=0>
              <input type="hidden" name="bookingId" value="<?= $data['booking']['bookingId']; ?>">
            </td>
            <td width="125"><button type="submit" class="btn btn-primary form-control">Catat</button></td>

          </form>
        </tr>
        <tr>
          <th>No. Urut</th>
          <th>Pos Biaya</th>
          <th>Uraian</th>
          <th>Banyaknya</th>
          <th>Index Harga</th>
          <th class="text-right">Jumlah Harga</th>
        </tr>
      </thead>
      <tbody>
        <?php $nu = 1;
        foreach ($data['danametu'] as $metu) : ?>
          <tr>

            <td class="text-right">
              <i class="fas fa-eraser crgOut" style="font-size: 24px; color:red; cursor:pointer" id="<?= $metu['notesId']; ?>"></i>
              <?= $nu; ?>
            </td>
            <td><?= $metu['posbiaya']; ?></td>
            <td><?= $metu['uraian']; ?></td>
            <td class="text-right"><?= $metu['quantity']; ?></td>
            <td class="text-right"><?= number_format($metu['indexHarga'], 2, ',', '.'); ?></td>
            <?php $jumlah = $metu['quantity'] * $metu['indexHarga']; ?>
            <td class="text-right"><?= number_format($jumlah, 2, ',', '.'); ?></td>
          </tr>
        <?php $nu++;
        endforeach; ?>
      </tbody>
    </table>
    <div class="card">
      <div class="card-header">
        <h3>Ikhtisar</h3>
      </div>
      <div class="card-body px-0 table-responsive">
        <h4>Pemasukan</h4>
        <table class="table table-sm table-striped">
          <thead>
            <tr>
              <th>No.</th>
              <th>Uraian</th>
              <th width="150">Jumlah</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td class="text-right">1.</td>
              <td>Paket (<?= $data['booking']['namaPaket']; ?> - include diskon)</td>
              <td class="text-right">
                <?= number_format($budget, 2, ',', '.'); ?></td>
            </tr>
            <tr>
              <td class="text-right">2.</td>
              <td>Biaya-biaya tambahan</td>
              <td class="text-right">
                <?= number_format($totalBiayaTambahan, 2, ',', '.'); ?></td>
            </tr>
            <tr>
              <td class="text-right">&nbsp;</td>
              <td>Total Pemasukan</td>
              <td class="text-right">
                <?= number_format(($totalBiayaTambahan + $budget), 2, ',', '.'); ?></td>
            </tr>
          </tbody>
        </table>

        <h4>Pengeluaran</h4>
        <?php $topeng = 0;
        $nmr = 1; ?>
        <table class="table table-sm table-striped">
          <thead>
            <tr>
              <th>No.</th>
              <th>Uraian</th>
              <th width="150">Jumlah</th>
            </tr>
          </thead>
          <tbody>
            <?php foreach ($data['ikhtisar'] as $expdt) : ?>
              <tr>
                <td class="text-right"><?= $nmr; ?>. </td>
                <td><?= $expdt['posbiaya']; ?></td>
                <td class="text-right"><?= number_format($expdt['totalHarga'], 2, ',', '.'); ?></td>
              </tr>
            <?php $topeng += $expdt['totalHarga'];
              $nmr++;
            endforeach; ?>
            <tr>
              <td>&nbsp;</td>
              <td>Total Pengeluaran</td>
              <td class="text-right"><?= number_format($topeng, 2, ',', '.'); ?></td>
            </tr>
          </tbody>
        </table>
        <table class="table table-sm table-bordered">
          <tbody>
            <tr>
              <td>
                <h4>Sisa Anggaran</h4>
              </td>
              <td width="150" class="text-right">
                <?php $sisa = ($totalBiayaTambahan + $budget) - $topeng;
                echo number_format($sisa, 2, ',', '.'); ?>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
</div>
<div class="row">
  <div class="col-lg-12">
    <div class="text-center">
      <a href="<?= BASEURL ?>Rafting/repExpdt/<?= $data['booking']['bookingId']; ?>" class="btn btn-primary"><i class="fas fa-print bg-primary"> Cetak</i></a>
    </div>
  </div>
</div>

<?php $this->view('template/bs4js'); ?>
<script>
  $('.crgIn').click(function() {
    let tenan = confirm("Biaya tambahan akan dihapus");
    if (tenan == true) {
      $.post("<?= BASEURL; ?>Rafting/reduceCrgIn", {
        chargeIdx: this.id
      }, function(resp) {
        if (resp == '1') {
          location.reload();
        }
      })
    }
  })

  $('.crgOut').click(function() {
    let bookingId, notesId, tenan;
    bookingId = "<?= $data['booking']['bookingId']; ?>";
    notesId = this.id;
    tenan = confirm('Catatan pengaluaran dibatalkan?');
    if (tenan == true) {
      $.post('<?= BASEURL . "Rafting/costCancelation"; ?>', {
        bookingId: bookingId,
        notesId: notesId
      }, function(resp) {
        if (resp == "1") {
          alert('Pencatatan dibatalkan');
          location.reload();
        }
      })
    }
  })

  $('.posbiaya').click(function() {
    let pos, uraian;
    uraian = $(this).text();
    pos = $(this).parent().parent().children('td:nth-child(1)').text();
    $('#posbiaya option[value="' + pos + '"]').prop('selected', true);
    $('#uraian').val(uraian);
    $.ajax({
      url: "<?= BASEURL; ?>Rafting/nomorCashOut/<?= $data['booking']['bookingId']; ?>",
      success: function(resp) {
        $('#notesId').val(resp);
        $('html,body').animate({
          scrollTop: $("#formCashOut").offset().top
        }, 'slow');
      }
    })

  })
</script>