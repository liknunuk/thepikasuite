<?php
// header("Content-type: application/vnd-ms-excel");
// header("Content-Disposition: attachment; filename=" . $data['booking']['bookingId'] . "-" . $data['booking']['namaPIC'] . ".xls");
?>
<div class="row">
    <div class="col-lg-12">
    <!-- Report: begin -->
    <table class="table table-sm table-bordered">
        <tbody>
        <tr>
            <td colspan="2">Nomor Booking</td>
            <td colspan="2"><?=$data['booking']['bookingId'];?></td>
            <td>Tarif / Pax</td>
            <td class="text-right"><?=number_format($data['booking']['price'],2,',','.');?></td>
        </tr>
        <tr>
            <td colspan="2">Tanggal</td>
            <td colspan="2"><?=$data['booking']['tanggalMulai'];?></td>
            <td>Tagihan</td>
            <td class="text-right"><?=number_format($data['booking']['billing'],2,',','.');?></td>
        </tr>
        <tr>
            <td colspan="2">Group/PIC</td>
            <td colspan="2"><?=$data['booking']['namaPIC'];?></td>
            <td>Diskon</td>
            <td class="text-right"><?=number_format($data['booking']['discount'],2,',','.');?></td>
        </tr>
        <tr>
            <td colspan="2">Jumlah Perserta</td>
            <td colspan="2"><?=$data['booking']['jumlahPerson'];?></td>
            <td>Total Anggaran</td>
            <td class="text-right">
            <?php 
            $budget = $data['booking']['billing'] - $data['booking']['discount'];
            echo number_format($budget,2,',','.');
            ?>
            </td>
        </tr>
        <tr>
            <td colspan="2">Paket Trip</td>
            <td colspan="2"><?=$data['booking']['namaPaket'];?></td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
        </tr>

        <!-- biaya tambahan  -->

        <tr>
            <td colspan="6">
            <h4>Penambahan Biaya / Additionan Charge</h4>
            </td>
        </tr>            
        <tr class="bg-secondary">
            <td colspan="3">Uraian</td>
            <td>Banyaknya</td>
            <td>Index Harga</td>
            <td>Jumlah Harga</td>
        </tr>
        <tbody>
        <?php 
        $totalBiayaTambahan = 0;
        foreach($data['danamara'] as $masuk): 
        ?>
        <tr>
            <td colspan="3"><span class="mr-3"></span><?=$masuk['uraian'];?></td>
            <td class="text-right"><?=$masuk['quantity'];?></td>
            <td class="text-right"><?=number_format($masuk['indexHarga'],2,',','.');?></td>
            <?php $jumlah = $masuk['quantity'] * $masuk['indexHarga'];?>
            <td class="text-right"><?=number_format($jumlah,2,',','.');?></td>
        </tr>
        <?php 
        $totalBiayaTambahan+=$jumlah;
        endforeach;
        ?>
        <tr>
            <th colspan="5">Sub Total Additional Charges</th>
            <th class="text-right"><?=number_format($totalBiayaTambahan,2,',','.'); ?></th>
        </tr>

        <!-- Pengeluaran -->

        <tr>
            <td colspan="6">
                <h4>Uraian Pengeluaran</h4>
            </td>
        </tr>

        <tr class="bg-secondary">
            <th>No. Urut</th>
            <th>Pos Biaya</th>
            <th>Uraian</th>
            <th>Banyaknya</th>
            <th>Index Harga</th>
            <th class="text-right">Jumlah Harga</th>
        </tr>
        <?php $nu=1; foreach($data['danametu'] as $metu): ?>
        <tr>
        
        <td class="text-right"><?=$nu;?></td>
        <td><?=$metu['posbiaya'];?></td>
        <td><?=$metu['uraian'];?></td>
        <td class="text-right"><?=$metu['quantity'];?></td>
        <td class="text-right"><?=number_format($metu['indexHarga'],2,',','.');?></td>
        <?php $jumlah = $metu['quantity'] * $metu['indexHarga']; ?>
        <td class="text-right"><?=number_format($jumlah,2,',','.');?></td>
        </tr>
        <?php $nu++; endforeach; ?>
        
        <!-- ikhtisar -->

        <tr><td colspan="6">&nbsp;</td></tr>
        <tr><td colspan="6">&nbsp;</td></tr>
        <tr>
            <td colspan="6">
                <h3>Ikhtisar</h3>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <h4>Pemasukan</h3>
            </td>
        </tr>

        <tr class="bg-secondary">
            <th>No.</th>
            <th colspan="4">Uraian</th>
            <th width="150" >Jumlah</th>
        </tr>
        <tr>
            <td class="text-right">1.</td>
            <td colspan="4">Paket (<?=$data['booking']['namaPaket'];?> - include diskon)</td>
            <td class="text-right">
            <?=number_format($budget,2,',','.');?></td>
        </tr>
        <tr>
            <td class="text-right">2.</td>
            <td colspan="4">Biaya-biaya tambahan</td>
            <td class="text-right">
            <?=number_format($totalBiayaTambahan,2,',','.');?></td>
        </tr>
        <tr>
            <td class="text-right">&nbsp;</td>
            <td colspan="4">Total Pemasukan</td>
            <td class="text-right">
            <?=number_format(($totalBiayaTambahan+$budget),2,',','.');?></td>
        </tr>
        <tr>
            <td colspan="6">
                <h4>Pengeluaran</h3>
            </td>
        </tr>

        <?php $nmr=1; foreach($data['ikhtisar'] as $expdt ): ?>
        <tr>
            <td class="text-right"><?=$nmr;?>. </td>
            <td colspan="4"><?=$expdt['posbiaya'];?></td>
            <td class="text-right"><?=number_format($expdt['totalHarga'],2,',','.');?></td>
        </tr>
        <?php $topeng+=$expdt['totalHarga']; $nmr++; endforeach; ?>
        <tr>
            <td>&nbsp;</td>
            <td colspan="4">Total Pengeluaran</td>
            <td class="text-right"><?=number_format($topeng,2,',','.');?></td>
        </tr>
        <tr>
            <td colspan="5">
            <h4>Sisa Anggaran</h4>
            </td>
            <td class="text-right">
            <?php $sisa = ($totalBiayaTambahan + $budget ) - $topeng; echo number_format($sisa,2,',','.');?>
            </td>
        </tr>
        </tbody>
    </table>

        <!-- Report: ended -->
    </div>
</div>

<?php $this->view('template/bs4js'); ?>
