<div class="row">
    <div class="col-lg-6 mt-2 mx-auto">
        <div class="page-title">
            <h3>Pengguna Sistem Pikas Management Suite</h3>
        </div>
        <div class="table-responsive">
            <?php Alert::sankil(); ?>
            <table class="table table-sm table-striped">
                <tbody>
                    <tr>
                        <th>User ID</th>
                        <td><?=$data['user']['userId'];?></td>
                    </tr>
                    <tr>
                        <th>Nama Lengkap</th>
                        <td><?=$data['user']['fullName'];?></td>
                    </tr>

                    <tr>
                        <th>Email</th>
                        <td><?=$data['user']['email'];?></td>
                    </tr>

                    <tr>
                        <th>Level</th>
                        <td><?=$this->posisiUser($data['user']['isadmin']);?></td>
                    </tr>
                    <tr>
                        <td colspan="2" class="text-center py-2">
                            <a href="javascript:void(0)" class="chpass" data-toggle="tooltip" data-placement="top" title="Ganti Password">
                                <i class="fas fa-key icon48"></i>
                            </a>
                            <a href="" class="rmuser" data-toggle="tooltip" data-placement="top" title="Hapus User">
                                <i class="fas fa-user-times icon48"></i>
                            </a>
                            <a href="javascript:void(0)" class="chname" data-toggle="tooltip" data-placement="top" title="Edit User">
                                <i class="fas fa-edit icon48"></i>
                            </a>
                            <a href="javascript:void(0)" id="<?=$data['user']['userId'].'_'.$data['user']['isadmin'];?>" class="swadmin">
                                <i class="fas fa-random icon48"></i>
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>

<!-- modals -->
<!-- modal ganti nama -->
<div class="modal" tabindex="-1" role="dialog" id="mdChName">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Ganti Nama</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="<?=BASEURL;?>User/chgName" method="post">
        <div class="form-group">
            <label for="mdchnameUserId">User ID</label>
            <input type="text" name="userId" id="mdchnameUserId" class="form-control" readonly value="<?=$data['user']['userId'];?>">
        </div>

        <div class="form-group">
            <label for="mdchnameFullName">Nama Lengkap</label>
            <input type="text" class="form-control" name="fullName" id="mdchnameFullName" value="<?=$data['user']['fullName'];?>">
        </div>

      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-primary">Simpan</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </form>
      </div>
    </div>
  </div>
</div>
<?php $this->view('template/bs4js'); ?>
<script>
$(document).ready(function () {
  $('[data-toggle="tooltip"]').tooltip()
})

// chpass , rmuser , chname
$('.chpass').on('click',function(){
    window.location="<?=BASEURL;?>User/userPass/<?=$data['user']['userId'];?>";
})
$('.rmuser').on('click',function(){
    let tenan = confirm('User akan dihapus');
    let userId = "<?=$data['user']['userId'];?>";
    if(tenan == true){
        $.post('<?=BASEURL;?>User/pecat' , {userId:userId} , function(resp){
            if ( resp == '1' ){
                window.location="<?=BASEURL;?>User";
            }else{
                location.reload();
            }
        })
    }
})

$('.chname').on('click',function(){
    $('#mdChName').modal('toggle');
})

$(".swadmin").on('click',function(){
    let data = this.id;
    let par, admin, isadmin, userId;
    par=data.split('_');
    admin = par[1]; userId=par[0];    
    if ( admin == '1' ){
        isadmin = '0';
    }else{
        isadmin = '1';
    }
    // alert (isadmin +' '+userId);
    $.post('<?=BASEURL;?>/User/swadmin',{ 
            isadmin : isadmin,
            userId  : userId
         }, function(resp){
            //  alert(resp);
        if(resp == '1' ) location.reload();
    });
})
</script>