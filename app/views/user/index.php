<div class="row">
    <div class="col-lg-12 mt-2">
        <div class="page-title">
            <h3>Pengguna Sistem Pikas Management Suite</h3>
        </div>
        <div class="table-responsive">
            <?php Alert::sankil(); ?>
            <table class="table table-sm table-striped">
                <thead>
                    <tr>
                        <th>User ID</th>
                        <th>Nama Lengkap</th>
                        <!-- <th>Email</th> -->
                        <th>Posisi</th>
                        <th class='text-center' width="50">Kontrol</th>
                    </tr>
                </thead>
                <tbody id="listOfUser">
                    <?php foreach($data['user'] as $user): ?>
                    <tr>
                        <td width="75"><?=$user['userId'];?></td>
                        <td><?=$user['fullName'];?></td>
                        <!-- <td><?=$user['email'];?></td> -->
                        <td>
                            <?=$this->posisiUser($user['isadmin']);?>
                            <a href="javascript:void(0)" id="<?=$user['userId'].'_'.$user['isadmin'];?>" class="swadmin">
                                <i class="fas fa-random">&nbsp;Ganti</i>
                            </a>
                        </td>
                        <td>
                            <a href="<?=BASEURL;?>User/info/<?=$user['userId'];?>">
                                <i class="fas fa-info-circle icon48"></i>
                            </a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>


<?php $this->view('template/bs4js'); ?>
<script>
$(".swadmin").on('click',function(){
    let data = this.id;
    let par, admin, isadmin, userId;
    par=data.split('_');
    admin = par[1]; userId=par[0];    
    if ( admin == '1' ){
        isadmin = '0';
    }else{
        isadmin = '1';
    }
    // alert (isadmin +' '+userId);
    $.post('<?=BASEURL;?>/User/swadmin',{ 
            isadmin : isadmin,
            userId  : userId
         }, function(resp){
            //  alert(resp);
        if(resp == '1' ) location.reload();
    });
})

$('#cariuser').on('click',function(){
    let fullName = $('#namauser').val();
    
    $.ajax({
        dataType: 'json',
        url     : "<?=BASEURL;?>User/cari/"+fullName,
        success : function(resp){
            $('#listOfUser tr').remove();
            $.each(resp , function(i,data){
                $('#listOfUser').append(`
                <tr>
                        <td width="75">${data.userId}</td>
                        <td>${data.fullName}</td>
                        <td>
                            &nbsp;
                        <td>
                            <a href="<?=BASEURL;?>User/info/${data.userId}">
                                <i class="fas fa-info-circle icon48"></i>
                            </a>
                        </td>
                    </tr>
                `)
            })
        }
    })
})
</script>